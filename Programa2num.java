package labs;

import java.util.Scanner;

public class Programa2num {
	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el primer número: ");
        double numero1 = scanner.nextDouble();

        System.out.print("Ingrese el segundo número: ");
        double numero2 = scanner.nextDouble();

        scanner.close();

        double resultado = (2 * numero1) + (numero2 * numero2);

        System.out.println("El resultado es: " + resultado);
    }
}
