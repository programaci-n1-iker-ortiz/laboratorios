package Prueba;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
public class Main {
	public static void main (String [] args) {
		Connection conexion = null;
		Statement statement = null;
		try {
			
			Class.forName("org.sqlite.JDBC");
			
			conexion = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\didie\\Documents\\DB SQLite\\ejemplo.db");
			
			statement = conexion.createStatement();
			
			statement.execute("INSERT INTO inicio (nombre, edad) VALUES ('Lucas', 25)");
			statement.execute("INSERT INTO inicio (nombre, edad) VALUES ('Luisa', 20)");
			statement.execute("INSERT INTO inicio (nombre, edad) VALUES ('Richar', 22)");
			
			ResultSet resultado = statement.executeQuery("SELECT * FROM inicio");
			System.out.println("Registros en la tabla inicio: ");
		    while (resultado.next()) {
		          int id = resultado.getInt("id");
		          String nombre = resultado.getString("nombre");
		          int edad = resultado.getInt("edad");
		         System.out.println("ID: " + id + ", Nombre: " + nombre + ",Edad: " + edad);
		       }
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (statement != null) statement.close();
					if (conexion != null) conexion.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	}
}
