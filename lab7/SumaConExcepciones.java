package lab7;

import java.util.Scanner;

public class SumaConExcepciones {
	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        try {
            System.out.print("Ingrese el primer número: ");
            int numero1 = scanner.nextInt();

            System.out.print("Ingrese el segundo número: ");
            int numero2 = scanner.nextInt();

            int suma = sumar(numero1, numero2);

            System.out.println("La suma es: " + suma);
        } catch (Exception e) {
            System.out.println("Error: Ingrese números enteros válidos.");
        }
    }

    public static int sumar(int numero1, int numero2) {
        return numero1 + numero2;
    }
}
