package Parcial;
import java.util.Scanner;

public class Gasolinera {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double litroMagna = 0;
        double litroPremium = 0;
        double litroDiesel = 0;
        double ventaMagna = 0;
        double ventaPremium = 0;
        double ventaDiesel = 0;
        
        double[][] litrosPorBomba = new double[3][4];
        double[][] ventasPorBomba = new double[3][4];
        
        System.out.println("       ¡Bienvenido a la Gasolinera! ");
        while (true) {
            System.out.println("Ingrese el tipo de combustible (Magna, Premium o Diesel): ");
            String tipoCombustible = scanner.nextLine();
            
            if (!tipoCombustible.equalsIgnoreCase("Magna") && 
                !tipoCombustible.equalsIgnoreCase("Premium") && 
                !tipoCombustible.equalsIgnoreCase("Diesel")) {
                System.out.println("Tipo de combustible no válido. Intente nuevamente.");
                continue;
            }
            
            System.out.println("¿Desea ingresar la cantidad en litros o en pesos? (litros/pesos): ");
            String metodoVenta = scanner.nextLine();
            
            if (!metodoVenta.equalsIgnoreCase("litros") && !metodoVenta.equalsIgnoreCase("pesos")) {
                System.out.println("Método de venta no válido. Intente nuevamente.");
                continue;
            }
            
            double litrosVender = 0;
            double importe = 0;
            
            if (metodoVenta.equalsIgnoreCase("litros")) {
                System.out.println("Ingrese la cantidad a vender en litros: ");
                litrosVender = scanner.nextDouble();
                importe = litrosVender * obtenerPrecioPorLitro(tipoCombustible);
            } else {
                System.out.println("Ingrese la cantidad a vender en pesos: ");
                importe = scanner.nextDouble();
                litrosVender = importe / obtenerPrecioPorLitro(tipoCombustible);
            }
            
            System.out.println("Importe a pagar: $" + importe);
            
            double cantidadPagada;
            do {
                System.out.println("Ingrese la cantidad pagada por el cliente: ");
                cantidadPagada = scanner.nextDouble();
                
                if (cantidadPagada < importe) {
                    System.out.println("La cantidad ingresada es insuficiente. Intente nuevamente.");
                }
            } while (cantidadPagada < importe);
            double cambio = cantidadPagada - importe;
            System.out.println("Cambio: $" + cambio);
            if (tipoCombustible.equalsIgnoreCase("Magna")) {
                litroMagna += litrosVender;
                ventaMagna += importe;
            } else if (tipoCombustible.equalsIgnoreCase("Premium")) {
                litroPremium += litrosVender;
                ventaPremium += importe;
            } else {
                litroDiesel += litrosVender;
                ventaDiesel += importe;
            }
            int isla = obtenerIsla(tipoCombustible);
            int bomba = obtenerBomba(tipoCombustible);
            litrosPorBomba[isla][bomba] += litrosVender;
            ventasPorBomba[isla][bomba] += importe;
            System.out.println("Venta exitosa. ¡Muchas Gracias por su compra!");
            System.out.println("¿Desea realizar otra venta? (S/N): ");
            String continuar = scanner.next();
            if (!continuar.equalsIgnoreCase("S")) {
                break;
            }
        }
        System.out.println("Fin del turno.");
        for (int isla = 0; isla < 3; isla++) {
            for (int bomba = 0; bomba < 4; bomba++) {
                System.out.println("Venta en Isla " + (isla + 1) + ", Bomba " + (bomba + 1));
                System.out.println("Total de litros vendidos: " + litrosPorBomba[isla][bomba] + " litros");
                System.out.println("Total de ventas: $" + ventasPorBomba[isla][bomba]);
            }
        }
        System.out.println("Total de litros vendidos de Magna: " + litroMagna + " litros");
        System.out.println("Total de litros vendidos de Premium: " + litroPremium + " litros");
        System.out.println("Total de litros vendidos de Diesel: " + litroDiesel + " litros");
        System.out.println("Total de ventas de Magna: $" + ventaMagna);
        System.out.println("Total de ventas de Premium: $" + ventaPremium);
        System.out.println("Total de ventas de Diesel: $" + ventaDiesel);
    }
    
    public static double obtenerPrecioPorLitro(String tipoCombustible) {
        double precioPorLitro = 0;
        
        if (tipoCombustible.equalsIgnoreCase("Magna")) {
            precioPorLitro = 2.3;
        } else if (tipoCombustible.equalsIgnoreCase("Premium")) {
            precioPorLitro = 5.2;
        } else if (tipoCombustible.equalsIgnoreCase("Diesel")) {
            precioPorLitro = 3.8;
        }
        
        return precioPorLitro;
    }
    
    public static int obtenerIsla(String tipoCombustible) {
        if (tipoCombustible.equalsIgnoreCase("Magna") || tipoCombustible.equalsIgnoreCase("Diesel")) {
            return 0;
        } else {
            return 1;
        }
    }
    
    public static int obtenerBomba(String tipoCombustible) {
        if (tipoCombustible.equalsIgnoreCase("Magna") || tipoCombustible.equalsIgnoreCase("Premium")) {
            return 0;
        } else {
            return 1; 
        }
    }
}