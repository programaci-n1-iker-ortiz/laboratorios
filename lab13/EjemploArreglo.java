package lab13;

public class EjemploArreglo {
	public static void main(String[] args) {
		 int[] numeros = new int[5];
		 numeros[0] = 1;
		 numeros[1] = 2;
		 numeros[2] = 3;
		 numeros[3] = 4;
		 numeros[4] = 5;
		 System.out.println("Elementos del arreglo:");
		 for (int i = 0; i < numeros.length; i++) {
		 System.out.println("Número en la posición " + i + ": " + 
		numeros[i]);
		 }
	}
}
