package lab13;

public class EjemploMatriz {
	public static void main(String[] args) {
		 int[][] matriz = new int[3][3];
		 for (int fila = 0; fila < 3; fila++) {
		 for (int columna = 0; columna < 3; columna++) {
		 matriz[fila][columna] = fila * 3 + columna + 1;
		 }
		 }
		 System.out.println("Elementos de la matriz:");
		 for (int fila = 0; fila < 3; fila++) {
		 for (int columna = 0; columna < 3; columna++) {
		 System.out.print(matriz[fila][columna] + " ");
		 }
		 System.out.println(); 
		 }
	 }
}
