package lab13;
import java.util.Scanner;
public class problema2 {
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Ingrese el tamaño del arreglo:");
        n = sc.nextInt();
        int[] arreglo = new int[n];
        System.out.println("Ingrese los valores del arreglo:");
        for (int i = 0; i < n; i++) {
            arreglo[i] = sc.nextInt();
        }
        int maximo = arreglo[0];
        for (int i = 1; i < n; i++) {
            if (arreglo[i] > maximo) {
                maximo = arreglo[i];
            }
        }
        System.out.println("El elemento máximo del arreglo es: " + maximo);
    }
}
