package labs;

import java.util.Scanner;

public class Valordelradio {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("Ingresa el valor del radio de la circunferencia: ");
	        double radio = scanner.nextDouble();

	        double longitud = 2 * Math.PI * radio;

	        double area = Math.PI * Math.pow(radio, 2);

	        System.out.println("Longitud de la circunferencia: " + longitud);
	        System.out.println("Área de la circunferencia: " + area);
	    }
}
