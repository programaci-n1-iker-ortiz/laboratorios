package labs;

import java.util.Scanner;

public class Programacifras {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("Ingresa un número entero de 5 cifras: ");
	        int numero = scanner.nextInt();

	        String numeroString = String.valueOf(numero);

	        for (int i = 1; i <= numeroString.length(); i++) {
	            System.out.println(numeroString.substring(0, i));
	        }
	    }
}
