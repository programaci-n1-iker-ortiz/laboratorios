package labs;

import java.util.Scanner;

public class Programagrados {
	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa la temperatura en grados Centigrados: ");
        double centigrados = scanner.nextDouble();

        double fahrenheit = 32 + (9 * centigrados / 5);

        System.out.println(centigrados + " grados centigrados equivalen a " + fahrenheit + " grados Fahrenheit.");
    }
}
