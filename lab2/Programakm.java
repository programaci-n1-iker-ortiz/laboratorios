package labs;

import java.util.Scanner;

public class Programakm {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("Ingresa la velocidad en Km/h: ");
	        double velocidadKmh = scanner.nextDouble();

	        double velocidadMs = velocidadKmh * (1000.0 / 3600.0);

	        System.out.println(velocidadKmh + " Km/h equivale a " + velocidadMs + " m/s.");
	    }
}
