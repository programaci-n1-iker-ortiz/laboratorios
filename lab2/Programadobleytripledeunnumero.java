package labs;

import java.util.Scanner;

public class Programadobleytripledeunnumero {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("Ingresa un número entero: ");
	        int numero = scanner.nextInt();

	        int doble = 2 * numero;
	        int triple = 3 * numero;

	        System.out.println("El doble de " + numero + " es: " + doble);
	        System.out.println("El triple de " + numero + " es: " + triple);
	    }
}
