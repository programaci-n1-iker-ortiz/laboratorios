package lab6;

import java.util.Scanner;

public class DivisionNumeros {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        
	        System.out.print("Ingrese el numerador: ");
	        int numerador = scanner.nextInt();
	        
	        System.out.print("Ingrese el denominador: ");
	        int denominador = scanner.nextInt();
	        
	        try {
	            double resultado = dividir(numerador, denominador);
	            System.out.println("El resultado de la división es: " + resultado);
	        } catch (ArithmeticException e) {
	            System.out.println("Error: División por cero no permitida.");
	        }
	    }
	    
	    public static double dividir(int numerador, int denominador) {
	        if (denominador == 0) {
	            throw new ArithmeticException("División por cero.");
	        }
	        return (double) numerador / denominador;
	    }
}
