package lab6;

import java.util.Scanner;

public class CuadradoNumero {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        
	        System.out.print("Ingrese un número entero: ");
	        int numero = scanner.nextInt();
	        
	        int cuadrado = numero * numero;
	        
	        System.out.println("El cuadrado de " + numero + " es: " + cuadrado);
	    }
}