package labs;

import java.util.Scanner;

public class Transformarpiesacm {
	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la cantidad en pies: ");
        double pies = scanner.nextDouble();

        scanner.close();

        double yardas = pies / 3.0; // 1 yarda = 3 pies
        double pulgadas = pies * 12.0; // 1 pie = 12 pulgadas
        double metros = pies * 0.3048; // 1 pie = 0.3048 metros
        double centimetros = metros * 100.0; // 1 metro = 100 centímetros

        System.out.println("Cantidad en yardas: " + yardas + " yardas");
        System.out.println("Cantidad en pulgadas: " + pulgadas + " pulgadas");
        System.out.println("Cantidad en metros: " + metros + " metros");
        System.out.println("Cantidad en centímetros: " + centimetros + " centímetros");
    }
}
