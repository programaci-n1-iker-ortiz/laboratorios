package labs;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Diasvividos {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("Ingrese su nombre: ");
	        String nombre = scanner.nextLine();

	        System.out.print("Ingrese su día de nacimiento (DD): ");
	        int diaNacimiento = scanner.nextInt();

	        System.out.print("Ingrese su mes de nacimiento (MM): ");
	        int mesNacimiento = scanner.nextInt();

	        System.out.print("Ingrese su año de nacimiento (AAAA): ");
	        int añoNacimiento = scanner.nextInt();

	        scanner.close();

	        LocalDate fechaActual = LocalDate.now();

	        LocalDate fechaNacimiento = LocalDate.of(añoNacimiento, mesNacimiento, diaNacimiento);

	        long diasVividos = ChronoUnit.DAYS.between(fechaNacimiento, fechaActual);

	        System.out.println(nombre + ", has vivido " + diasVividos + " días hasta hoy.");
	    }
}
