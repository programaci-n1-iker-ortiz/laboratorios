package labs;

import java.util.Scanner;

public class Clase {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("Ingrese la cantidad en dólares: ");

	        double dolares = scanner.nextDouble();

	        scanner.close();

	        int centavos = (int) (dolares * 100);

	        System.out.println("La cantidad en centavos es: " + centavos + " centavos");
	    }
}
