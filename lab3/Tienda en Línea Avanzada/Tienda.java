package lab3prueba;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Tienda {
	 ArrayList<Producto> inventario;
	    private Map<String, Cliente> clientes;

	    public Tienda() {
	        this.inventario = new ArrayList<>();
	        this.clientes = new HashMap<>();
	    }

	    public void agregarProducto(Producto producto) {
	        inventario.add(producto);
	    }

	    public void agregarCliente(Cliente cliente) {
	        clientes.put(cliente.getNombre(), cliente);
	    }

	    public void imprimirInventario() {
	        System.out.println("Inventario:");
	        for (Producto producto : inventario) {
	            System.out.println(producto.getNombre() + " - $" + producto.getPrecio());
	        }
	    }

	    public Cliente buscarCliente(String nombre) {
	        return clientes.get(nombre);
	    }
}
