package lab3prueba;

import java.util.Scanner;

public class Main {
	 public static void main(String[] args) {
	        Tienda tienda = new Tienda();

	        tienda.agregarProducto(new Producto("lentes", 10.0));
	        tienda.agregarProducto(new Producto("sudadera", 20.0));
	        tienda.agregarProducto(new Producto("zapatillas", 30.0));
	        tienda.agregarProducto(new Producto("medias", 5.0));
	        tienda.agregarProducto(new Producto("sueter", 15.0));
	        
	        Cliente cliente1 = new Cliente("iker");
	        Cliente cliente2 = new Cliente("cindy");

	        tienda.agregarCliente(cliente1);
	        tienda.agregarCliente(cliente2);

	        try (
			Scanner scanner = new Scanner(System.in)) {
				System.out.println("Bienvenido a la tienda en línea");

				System.out.print("Nombre de cliente: ");
				String nombreCliente = scanner.nextLine();

				Cliente clienteActual = tienda.buscarCliente(nombreCliente);

				if (clienteActual != null) {
				    System.out.println("Hola " + clienteActual.getNombre());

				    tienda.imprimirInventario();

				    System.out.print("¿Desea agregar un producto al carrito? (S/N): ");
				    String opcion = scanner.nextLine();

				    while (opcion.equalsIgnoreCase("S")) {
				        System.out.print("Nombre del producto: ");
				        String nombreProducto = scanner.nextLine();

				        Producto productoSeleccionado = null;
				        for (Producto producto : tienda.inventario) {
				            if (producto.getNombre().equalsIgnoreCase(nombreProducto)) {
				                productoSeleccionado = producto;
				                break;
				            }
				        }

				        if (productoSeleccionado != null) {
				            clienteActual.agregarAlCarrito(productoSeleccionado);
				        } else {
				            System.out.println("Producto no encontrado");
				        }

				        System.out.print("¿Desea agregar otro producto al carrito? (S/N): ");
				        opcion = scanner.nextLine();
				    }

				    System.out.print("¿Desea eliminar un producto del carrito? (S/N): ");
				    opcion = scanner.nextLine();

				    while (opcion.equalsIgnoreCase("S")) {
				        System.out.print("Nombre del producto a eliminar: ");
				        String nombreProductoEliminar = scanner.nextLine();

				        Producto productoAEliminar = null;
				        for (Producto producto : clienteActual.carrito.getProductos()) {
				            if (producto.getNombre().equalsIgnoreCase(nombreProductoEliminar)) {
				                productoAEliminar = producto;
				                break;
				            }
				        }

				        if (productoAEliminar != null) {
				            clienteActual.eliminarDelCarrito(productoAEliminar);
				            System.out.println("Producto eliminado del carrito.");
				        } else {
				            System.out.println("Producto no encontrado en el carrito.");
				        }

				        System.out.print("¿Desea eliminar otro producto del carrito? (S/N): ");
				        opcion = scanner.nextLine();
				    }

				    clienteActual.procesarPago();
				    clienteActual.imprimirComprasAnteriores();
				} else {
				    System.out.println("Cliente no encontrado");
				}
			}
	    }
}
