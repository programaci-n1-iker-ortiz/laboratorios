package lab3prueba;

import java.util.ArrayList;

public class Cliente {
	private String nombre;
    private ArrayList<Producto> comprasAnteriores;
    CarritoCompras carrito;

    public Cliente(String nombre) {
        this.nombre = nombre;
        this.comprasAnteriores = new ArrayList<>();
        this.carrito = new CarritoCompras();
    }

    public String getNombre() {
        return nombre;
    }

    public void agregarAlCarrito(Producto producto) {
        carrito.agregarProducto(producto);
    }

    public void eliminarDelCarrito(Producto producto) {
        carrito.eliminarProducto(producto);
    }

    public void procesarPago() {
        double total = carrito.calcularTotal();
        System.out.println("Total a pagar: $" + total);
        comprasAnteriores.addAll(carrito.getProductos());
        carrito.vaciarCarrito();
    }

    public void imprimirComprasAnteriores() {
        System.out.println("Compras anteriores de " + nombre + ":");
        for (Producto producto : comprasAnteriores) {
            System.out.println(producto.getNombre() + " - $" + producto.getPrecio());
        }
    }
}
