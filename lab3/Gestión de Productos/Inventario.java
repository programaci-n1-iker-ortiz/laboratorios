package Gestiondeproductos;
import java.util.ArrayList;
import java.util.List;
public class Inventario {
private List<Producto> productos;
	public Inventario() {
		this.productos = new ArrayList<>();
	}
		public void agregarProducto(Producto producto) {
			this.productos.add(producto);
		}
		public void eliminarProducto(String nombre) {
			for (Producto producto : productos) {
				if (producto.getNombre().equals(nombre)) {
					this.productos.remove(producto);
					return;
				}
			}
		}
		public void actualizarProducto(String nombre, double Precio, int stock) {
			for (Producto producto: productos ){
				if (producto.getNombre().equals(nombre)) {
					producto.setPrecio(Precio);
			        producto.setstock(stock);
			        return;
				}
			}
		}
		public List<Producto> getProductos(){
			return this.productos;
		}
}
