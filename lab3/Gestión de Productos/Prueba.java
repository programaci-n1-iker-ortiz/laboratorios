package Gestiondeproductos;

public class Prueba {
	public static void main(String[] args) {
        Producto producto1 = new Producto("Cable A", 6.99, 33);
        Producto producto2 = new Producto("Cable B", 5.99, 17);
        Inventario inventario = new Inventario();
        inventario.agregarProducto(producto1);
        inventario.agregarProducto(producto2);
        System.out.println("Productos en inventario:");
        for (Producto producto :  inventario.getProductos()) {
            System.out.println("Nombre: " + producto.getNombre()+", Precio: "+producto.getPrecio()+", Stock: "+producto.getStock());
        }
        inventario.eliminarProducto("Cable B");
        System.out.println("\nProductos después de eliminar Cable B:");
        for (Producto producto : inventario.getProductos()) {
            System.out.println("Nombre: " + producto.getNombre()+", Precio: "+producto.getPrecio()+", Stock: "+producto.getStock());
        }
        inventario.actualizarProducto("Cable A", 7.99, 7);
        System.out.println("\nProductos después de actualizar Cable A:");
        for (Producto producto : inventario.getProductos()) {
            System.out.println("Nombre: " + producto.getNombre()+", Precio: "+producto.getPrecio()+", Stock: "+producto.getStock());
        }
    }

}
