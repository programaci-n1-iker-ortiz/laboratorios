package lab4;

public class Coche {
	 private String marca;
	    private String modelo;
	    private int ano;
	    
	    public Coche(String marca, String modelo, int ano) {
	        this.marca = marca;
	        this.modelo = modelo;
	        this.ano = ano;
	    }
	    
	    public void setMarca(String marca) {
	        this.marca = marca;
	    }
	    
	    public String getMarca() {
	        return marca;
	    }
	    
	    public void setModelo(String modelo) {
	        this.modelo = modelo;
	    }
	    
	    public String getModelo() {
	        return modelo;
	    }
	    
	    public void setAno(int ano) {
	        this.ano = ano;
	    }
	    
	    public int getAno() {
	        return ano;
	    }
	    
	    public void arrancar() {
	        System.out.println("El coche " + marca + " " + modelo + " del año " + ano + " ha arrancado.");
	    }

	    public static void main(String[] args) {
	        Coche miCoche = new Coche("Toyota", "Corolla", 2022);
	        
	        miCoche.arrancar();
	    }
}
