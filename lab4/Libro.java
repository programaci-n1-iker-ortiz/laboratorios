package lab4;

public class Libro {
	 private String titulo;
	    private String autor;
	    private int anoPublicacion;
	    
	    public Libro(String titulo, String autor, int anoPublicacion) {
	        this.titulo = titulo;
	        this.autor = autor;
	        this.anoPublicacion = anoPublicacion;
	    }
	    
	    public void setTitulo(String titulo) {
	        this.titulo = titulo;
	    }
	    
	    public String getTitulo() {
	        return titulo;
	    }
	    
	    public void setAutor(String autor) {
	        this.autor = autor;
	    }
	    
	    public String getAutor() {
	        return autor;
	    }
	    
	    public void setAnoPublicacion(int anoPublicacion) {
	        this.anoPublicacion = anoPublicacion;
	    }
	    
	    public int getAnoPublicacion() {
	        return anoPublicacion;
	    }
	    
	    public void imprimirDetalles() {
	        System.out.println("Título: " + titulo);
	        System.out.println("Autor: " + autor);
	        System.out.println("Año de Publicación: " + anoPublicacion);
	    }

	    public static void main(String[] args) {
	        Libro miLibro = new Libro("El Principito", "Julian Alvarado", 1966);
	        miLibro.imprimirDetalles();
	    }
}
