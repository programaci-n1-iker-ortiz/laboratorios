package lab4;

public class Estudiante {
	private String nombre;
    private int edad;
    private String numeroIdentificacion;
    
    public Estudiante(String nombre, int edad, String numeroIdentificacion) {
        this.nombre = nombre;
        this.edad = edad;
        this.numeroIdentificacion = numeroIdentificacion;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public int getEdad() {
        return edad;
    }
    
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }
    
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }
    
    public void imprimirInformacion() {
        System.out.println("Nombre: " + nombre);
        System.out.println("Edad: " + edad);
        System.out.println("Número de Identificación: " + numeroIdentificacion);
    }

    public static void main(String[] args) {
        Estudiante estudiante = new Estudiante("Josue Rodriguez", 23, "8-1863-3672");
        estudiante.imprimirInformacion();
    }
}
