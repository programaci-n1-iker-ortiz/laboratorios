package lab4;

public class Tienda {
	private String nombreTienda;
    private String direccion;
    
    public Tienda(String nombreTienda, String direccion) {
        this.nombreTienda = nombreTienda;
        this.direccion = direccion;
    }
    
    public void venderProducto(String nombreProducto, double precio) {
        System.out.println("Venta realizada en la tienda " + nombreTienda + ".");
        System.out.println("Producto vendido: " + nombreProducto);
        System.out.println("Precio: $" + precio);
    }

    public static void main(String[] args) {
        Tienda miTienda = new Tienda("Mi Tienda", "Calle Principal, 123");
        
        miTienda.venderProducto("Camiseta", 20.0);
        miTienda.venderProducto("Zapatos", 50.0);
    }
}
