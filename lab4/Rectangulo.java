package lab4;

public class Rectangulo {
	 private double longitud;
	    private double ancho;
	    
	    public Rectangulo(double longitud, double ancho) {
	        this.longitud = longitud;
	        this.ancho = ancho;
	    }
	    
	    public double calcularArea() {
	        return longitud * ancho;
	    }
	    
	    public double calcularPerimetro() {
	        return 2 * (longitud + ancho);
	    }

	    public static void main(String[] args) {
	        Rectangulo miRectangulo = new Rectangulo(5.0, 3.0);
	        
	        double area = miRectangulo.calcularArea();
	        double perimetro = miRectangulo.calcularPerimetro();
	        
	        System.out.println("Área del rectángulo: " + area);
	        System.out.println("Perímetro del rectángulo: " + perimetro);
	    }
}
