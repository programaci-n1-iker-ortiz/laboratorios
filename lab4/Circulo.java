package lab4;

public class Circulo {
private double radio;
    
    public Circulo(double radio) {
        this.radio = radio;
    }
    
    public double calcularArea() {
        return Math.PI * Math.pow(radio, 2);
    }
    
    public double calcularPerimetro() {
        return 2 * Math.PI * radio;
    }

    public static void main(String[] args) {
        Circulo miCirculo = new Circulo(5.0);
        
        double area = miCirculo.calcularArea();
        double perimetro = miCirculo.calcularPerimetro();
        
        System.out.println("Área del círculo: " + area);
        System.out.println("Perímetro del círculo: " + perimetro);
    }
}
