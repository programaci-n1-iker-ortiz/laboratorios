package lab12;
import java.util.Scanner;
public class problema3 {
	 public static void main(String[] args) {
	        Scanner sc = new Scanner(System.in);
	        System.out.println("Ingrese la nota del estudiante:");
	        double nota = sc.nextDouble();
	        if (nota >= 0 && nota <= 100) {
	            if (nota >= 91) {
	                System.out.println("La calificación correspondiente a la nota " + nota + " es A.");
	            } else if (nota >= 81) {
	                System.out.println("La calificación correspondiente a la nota " + nota + " es B.");
	            } else if (nota >= 71) {
	                System.out.println("La calificación correspondiente a la nota " + nota + " es C.");
	            } else if (nota >= 61) {
	                System.out.println("La calificación correspondiente a la nota " + nota + " es D.");
	            } else {
	                System.out.println("La calificación correspondiente a la nota " + nota + " es F.");
	            }
	        } else {
	            System.out.println("La nota ingresada no es válida.");
	        }
	    }
}
