package lab12;
import java.util.Scanner;
public class problema4 {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        do {
	            System.out.println("MENU PRINCIPAL");
	            System.out.println("1. Problema 1");
	            System.out.println("2. Problema 2");
	            System.out.println("3. Problema 3");
	            System.out.println("4. Salir");
	            System.out.print("Seleccione una opción: ");

	            int opcion = scanner.nextInt();

	            switch (opcion) {
	                case 1:
	                    System.out.println("Escogiste la opción 1 - Problema 1");
	                    resolverProblema1();
	                    break;
	                case 2:
	                    System.out.println("Escogiste la opción 2 - Problema 2");
	                    resolverProblema2();
	                    break;
	                case 3:
	                    System.out.println("Escogiste la opción 3 - Problema 3");
	                    resolverProblema3();
	                    break;
	                case 4:
	                    System.out.println("Saliendo del programa. ¡Hasta luego!");
	                    scanner.close();
	                    System.exit(0);
	                default:
	                    System.out.println("Opción no válida. Por favor, seleccione una opción válida.");
	            }

	            System.out.print("¿Desea elegir otra opción? (Sí/No): ");
	        } while (scanner.next().equalsIgnoreCase("Sí"));

	        System.out.println("Saliendo del programa. ¡Hasta luego!");
	        scanner.close();
	    }


	    private static void resolverProblema1() {
	        System.out.println("Implementación del Problema 1");
	    }

	    private static void resolverProblema2() {
	        System.out.println("Implementación del Problema 2");
	    }

	    private static void resolverProblema3() {
	        System.out.println("Implementación del Problema 3");
	    }
}
