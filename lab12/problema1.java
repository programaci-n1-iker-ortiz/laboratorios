package lab12;

import java.util.Scanner;

public class problema1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingresa el primer número:");
        int numero1 = (int) scanner.nextDouble();

        System.out.println("Ingresa el segundo número:");
        int numero2 = (int) scanner.nextDouble();

        System.out.println("Ingresa el tercer número:");
        int numero3 = (int) scanner.nextDouble();

        int maximo = (int) calcularMaximo(numero1, numero2, numero3);

        System.out.println("El máximo de los tres números es: " + maximo);

        scanner.close();
    }

    public static double calcularMaximo(int a, int b, int c) {
        return Math.max(Math.max(a, b), c);
    }
}

