package lab9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RegistroForm extends JFrame implements ActionListener {
    JLabel userLabel, passwordLabel, emailLabel, messageLabel;
    JTextField userField, emailField;
    JPasswordField passwordField;
    JButton registerButton;

    public RegistroForm() {
        setTitle("Registro");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(5, 2));

        userLabel = new JLabel("Usuario:");
        passwordLabel = new JLabel("Contraseña:");
        emailLabel = new JLabel("Correo electrónico:");
        messageLabel = new JLabel("");
        userField = new JTextField();
        passwordField = new JPasswordField();
        emailField = new JTextField();
        registerButton = new JButton("Registrarse");

        add(userLabel);
        add(userField);
        add(passwordLabel);
        add(passwordField);
        add(emailLabel);
        add(emailField);
        add(new JLabel()); 
        add(registerButton);
        add(messageLabel); 

        registerButton.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        String usuario = userField.getText();
        String contraseña = new String(passwordField.getPassword());
        String correo = emailField.getText();

        if (!usuario.isEmpty() && !contraseña.isEmpty() && !correo.isEmpty()) {
            messageLabel.setText("Registro exitoso");
        } else {
            messageLabel.setText("Por favor, completa todos los campos.");
        }
    }

    public static void main(String[] args) {
        RegistroForm registroForm = new RegistroForm();
        registroForm.setVisible(true);
    }
}
