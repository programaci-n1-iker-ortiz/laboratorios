package lab9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class LoginForm extends JFrame implements ActionListener {
    JLabel userLabel, passwordLabel, messageLabel;
    JTextField userField;
    JPasswordField passwordField;
    JButton loginButton;

    public LoginForm() {
        setTitle("Inicio de Sesión");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(3, 2));

        userLabel = new JLabel("Usuario:");
        passwordLabel = new JLabel("Contraseña:");
        messageLabel = new JLabel("");
        userField = new JTextField();
        passwordField = new JPasswordField();
        loginButton = new JButton("Iniciar Sesión");

        add(userLabel);
        add(userField);
        add(passwordLabel);
        add(passwordField);
        add(messageLabel);
        add(loginButton);

        loginButton.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        String usuario = userField.getText();
        String contraseña = new String(passwordField.getPassword());

        if (usuario.equals("iker") && contraseña.equals("12345")) {
            messageLabel.setText("Inicio de sesión exitoso");
        } else {
            messageLabel.setText("Credenciales incorrectas. Intente de nuevo.");
        }
    }

    public static void main(String[] args) {
        LoginForm loginForm = new LoginForm();
        loginForm.setVisible(true);
    }
}

