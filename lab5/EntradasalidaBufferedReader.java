package Lab5;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class EntradasalidaBufferedReader {
	public static void main(String[]args) {
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		try {
			System.out.print("Ingresa tu nombre: ");
			String nombre = br.readLine();
			System.out.println("Hola, " + nombre +"!");
		}catch (IOException e) {
			System.err.println("Error de entrada/salida: "+e.getMessage());
		}finally {
			try {
			if (br != null) {
				br.close();
			}
			}catch (IOException e){
				System.err.print("Error al cerrar BufferedReader:"+e.getMessage());
			}
		}
	}
}