package lab8;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class formularioSimple {
	public static void main(String[] args) {
		 JFrame frame = new JFrame("Ejemplo de Formulario");
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setSize(300, 200);
		 JPanel panel = new JPanel();
		 JTextField textField = new JTextField(20);
		 JButton button = new JButton("Enviar");
		 button.addActionListener(new ActionListener() {
		 @Override
		 public void actionPerformed(ActionEvent e) {
		 String texto = textField.getText();
		 JOptionPane.showMessageDialog(null, "Texto ingresado: " + 
		texto);
		 }
		 });
		 panel.add(textField);
		 panel.add(button);
		 frame.add(panel);
		 frame.setVisible(true);
		 }
}
